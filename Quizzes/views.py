from datetime import datetime

from django.shortcuts import render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from Quizzes.models import Question, Result, QuizzesPython
from django.contrib.auth.decorators import login_required


class RegisterFormView(FormView):
    form_class = UserCreationForm

    # Ссылка, на которую будет перенаправляться пользователь в случае успешной регистрации.
    # В данном случае указана ссылка на страницу входа для зарегистрированных пользователей.
    success_url = "/Quizzes/home/"

    # Шаблон, который будет использоваться при отображении представления.
    template_name = "Quizzes/register.html"

    def form_valid(self, form):
        # Создаём пользователя, если данные в форму были введены корректно.
        form.save()

        # Вызываем метод базового класса
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    # form_class = AuthenticationForm
    form_class = AuthenticationForm
    # template:
    template_name = "Quizzes/home.html"
    # redirect to:
    success_url = "/Quizzes/register/"

    def form_invalid(self, form):
        # Получаем объект пользователя на основе введённых в форму данных.
        self.user = form.get_user()
        # Выполняем аутентификацию пользователя.
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


@login_required(login_url='../../Quizzes/home')
def logout_view(request):
    return render(request, 'Quizzes/user_home.html')


def home(request):
    return render(request, 'Quizzes/home.html')


def css(request):
    return render(request, 'Quizzes/static/css/main.css', content_type='text/css')


@login_required(login_url='../../Quizzes/home')
def python_quizzes(request):
    """Возвращает объекты всех существующих опросников"""
    quizzes = QuizzesPython.objects.all()
    context = {
        'quizzes': quizzes
    }
    return render(request, 'Quizzes/python.html', context)


@login_required(login_url='../../Quizzes/home')
def select_quiz(request):
    """Возвращает объект выбранного опросника."""
    quiz = QuizzesPython.objects.filter(id=request.POST.get('quiz', False)).first()
    if quiz:
        context = {
            'quiz': quiz
        }
        return render(request, 'Quizzes/python_quiz.html', context)
    else:
        return python_quizzes(request)


@login_required(login_url='../../Quizzes/home')
def quiz_save(request):
    """
    Сохраняет результаты пройденного опросника
    и перенаправляет на страницу с результатом
    этого опросника.
    """
    if request.method == 'POST':
        quiz = QuizzesPython.objects.filter(id=request.POST.get('quiz_id', False)).first()
        now = datetime.now()
        for field, value in request.POST.items():
            parts = field.split('_')
            if parts[0] != 'question':
                continue
            result = Result(
                user=request.user,
                id_quizzes=quiz,
                question=parts[1],
                answer=value,
                date=now
            )
            result.save()
        results = Result.objects.filter(date=now).all()
        final_results = {}
        for result in results:
            final_results.setdefault((result.id_quizzes, result.question), {
                'answers': set(),
            })
            final_results[result.id_quizzes, result.question]['answers'].add(result.answer)
            for (id_quizzes, question_id), result in final_results.items():
                question, = Question.objects.filter(id=question_id)
                result['correct_answer'] = ', '.join(
                    sorted(answer.title for answer in question.id_answer.all() if answer.status))
                result['question'] = question.title
                result['quizzes_name'] = id_quizzes.title
                result['answer'] = ', '.join(sorted(result['answers']))
                if result['answer'] == result['correct_answer']:
                    result['score'] = 1
                else:
                    result['score'] = 0
        context = {
            'result': final_results.values()
        }
        return render(request, 'Quizzes/successful_completion.html', context)
    else:
        return render(request, 'Quizzes/python_quiz.html')


@login_required(login_url='../../Quizzes/home')
def results(request):
    """Возвращает объект Result по выбранной дате."""
    if request.method == 'POST':
        results_for_user = Result.objects.filter(date=request.POST.get('date', False))
        final_results = {}
        for result in results_for_user:
            final_results.setdefault((result.id_quizzes, result.question), {
                'answers': set(),
            })
            final_results[result.id_quizzes, result.question]['answers'].add(result.answer)
        for (id_quizzes, question_id), result in final_results.items():
            question, = Question.objects.filter(id=question_id)
            result['correct_answer'] = ', '.join(sorted(
                answer.title for answer in question.id_answer.all() if answer.status)
            )
            result['question'] = question.title
            result['quizzes_name'] = id_quizzes.title
            result['answer'] = ', '.join(sorted(result['answers']))
            if result['answer'] == result['correct_answer']:
                result['score'] = 1
            else:
                result['score'] = 0

        context = {
            'results': final_results.values()
        }
        return render(request, 'Quizzes/result.html', context)


@login_required(login_url='../../Quizzes/home')
def select_quizzes_results(request):
    """
    Если пользователь входить в группу privileges,
    то функция возвращает список пользователей, которые
    прошли хотя бы один опросник. Если пользователь
    не входит в эту группу, то возвращает список опросников,
    который проходил пользователь.
    """
    if request.user.groups.filter(name='privileges'):
        users = Result.objects.all()
        all_users = list()
        for user in users:
            if user.user not in all_users:
                all_users.append(user.user)
        context = {
            'quizzes': all_users,
            'username': request.user
        }
        return render(request, 'Quizzes/select_user.html', context)
    else:
        user_results = Result.objects.filter(user=request.user)
        if user_results:
            user_quizzes = list()
            for user_result in user_results:
                if user_result.id_quizzes not in user_quizzes:
                    user_quizzes.append(user_result.id_quizzes)
            context = {
                'quizzes': user_quizzes,
                'user': user_results[1].user,
                'username': request.user
            }
            return render(request, 'Quizzes/select_results.html', context)
        else:
            return render(request, 'Quizzes/select_results.html')


@login_required(login_url='../../Quizzes/home')
def result_for_user(request):
    """
    Для пользователя с правами на просмотр всех результатов,
    возращает список опросников, которые прошел выбранный пользователь.
    """
    if request.method == 'POST':
        user = request.POST.get('user')
        if user:
            user_results = Result.objects.filter(user=user)
            user_quizzes = list()
            for user_result in user_results:
                if user_result.id_quizzes not in user_quizzes:
                    user_quizzes.append(user_result.id_quizzes)
            context = {
                'quizzes': user_quizzes,
                'user': user,
                'username': request.user
            }
            return render(request, 'Quizzes/select_results.html', context)
        else:
            return render(request, 'Quizzes/select_results.html')


@login_required(login_url='../../Quizzes/home')
def select_date_results(request):
    """
    Возвращает список дат, по выбранному опроснику
    и выбранному пользователю
    """
    if request.method == 'POST':
        if request.user.groups.filter(name='privileges'):
            user = request.POST.get('user')
        else:
            user = request.user
        result_dates = Result.objects.filter(id_quizzes=request.POST.get('quiz'), user=user).all()
        dates_quizzes = {result_date.date for result_date in result_dates}
        context = {
            'dates': list(dates_quizzes),
            'username': request.user,
            'user': request.POST.get('user')
        }
        return render(request, 'Quizzes/dates_for_result.html', context)
