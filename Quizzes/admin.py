from django.contrib import admin
from Quizzes.models import Question, Answer, QuizzesPython,  Result


class QuestionAdmin(admin.ModelAdmin):
    model = Question
    filter_horizontal = ('id_answer',)


class QuizzesPythonAdmin(admin.ModelAdmin):
    model = QuizzesPython
    filter_horizontal = ('id_question',)


admin.site.register(Answer)
admin.site.register(QuizzesPython, QuizzesPythonAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Result)
