# Generated by Django 2.0.3 on 2018-05-30 15:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quizzes', '0020_auto_20180423_2148'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='correct_answer',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
