# Generated by Django 2.0.3 on 2018-04-23 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quizzes', '0019_auto_20180423_1609'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CorrectAnswer',
        ),
        migrations.AddField(
            model_name='answer',
            name='status',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
