from functools import partial

from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^register/$', views.RegisterFormView.as_view()),
    url(r'^logout/$', partial(auth_views.logout, template_name='Quizzes/home.html'), name='logout'),
    url(r'^admin/', admin.site.urls),
    url(r'^home/$', partial(auth_views.login, template_name='Quizzes/home.html'), name='login'),
    url(r'^user_home/$', views.logout_view),
    url(r'css/main.css', views.css),
    url(r'python/$', views.python_quizzes),
    url(r'python_quiz/$', views.select_quiz),
    url(r'quiz_save/$', views.quiz_save),
    url(r'result/$', views.results),
    url(r'select_results/$', views.select_quizzes_results),
    url(r'select_date/$', views.select_date_results),
    url(r'select_user/$', views.result_for_user),
]