from datetime import datetime

from django.db import models
from django.contrib.auth.models import User


class Answer(models.Model):
    title = models.TextField(
        verbose_name='Ответ на вопрос'
    )
    status = models.BooleanField(
        verbose_name='Является ли правильным ответом'
    )


class Question(models.Model):
    title = models.TextField(
        verbose_name='Вопрос'
    )
    id_answer = models.ManyToManyField(Answer)
    QUESTION_TYPE_CHOISES = (
        ('1', 'checkbox'),
        ('2', 'radio'),
        ('3', 'text'),
    )
    question_type = models.CharField(
        max_length=30,
        choices=QUESTION_TYPE_CHOISES,
        default=1,
        verbose_name='Тип вопроса'
    )


class QuizzesPython(models.Model):
    id_question = models.ManyToManyField(Question)
    title = models.CharField(
        max_length=20,
        verbose_name='Название опросника'
    )


class Result(models.Model):
    id_quizzes = models.ForeignKey(QuizzesPython, on_delete=models.CASCADE)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='Пользователь')
    question = models.TextField(
        verbose_name='Номер вопроса'
    )
    answer = models.TextField(
        verbose_name='Ответ пользователя'
    )
    date = models.TextField(
        verbose_name='Дата завершения опросника'
    )
